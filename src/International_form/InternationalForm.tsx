import * as React from "react";
import {useState} from "react"
import "./internationalForm.css"
import MultiCity from "./MultiCity";
import CustomSelectionInput from "./CustomSelectionInput";
import CustomDateSelection from "./CustomDateSelection";
import {SwapOutlined} from "@ant-design/icons"
import CustomPeopleSelection from "./CustomPeopleSelection";

const InternationalForm = () => {
   const [multiCity, setMultiCity] = useState(false);
   const multiCityClickHandler = () => {
      // Set multicity to opposit value
      setMultiCity((pre)=> !pre)
   }
   const [dataFrom, setDataFrom] = useState("");
   const [dataTo, setDataTo] = useState("");
   const [dataFlightDate, setFlightDate] = useState("");
   const [dataReturnDate, setReturnDate] = useState("");
   const [dataNationality, setNationality] = useState("");
   const [dataPeople, setPeople] = useState({adult:1, child:0});
   const [multiCityData, setMultiCityData] = useState([
         {from: '', to: '', date:''}, 
         {from: '', to: '', date:''}])

   const sampleCityList = ["Bhojpur", "Birgunh", "Hetauda", 
      "Ilam", "Kathmandu"]
   return <form id="internationalForm">
      <div className="form-group multi-city" onClick={multiCityClickHandler}>
         <input type="checkbox" name="multiCity" checked={multiCity} />
         <label htmlFor="multiCity">Multi City</label>
      </div>
      {/* Display multicicy section if checkbox is ticked */}
      {multiCity? (<MultiCity data={multiCityData} setData={setMultiCityData} />):
      // Else show default destiantion selection
      (<div className="form-group destination-group">
         <div className="">
            <CustomSelectionInput width="140px" title="From" data={dataFrom} 
               setData={setDataFrom} selectionData={sampleCityList}/>
            <SwapOutlined style={{color: "#bbb", margin: "0px 10px"}} />
            <CustomSelectionInput width="140px" title="To" data={dataTo} 
               setData={setDataTo} selectionData={sampleCityList}/>
            </div>
         <CustomDateSelection title="Flight Date" data={dataFlightDate} 
            setData={setFlightDate} />
         <CustomDateSelection title="Return Date" data={dataReturnDate} 
            setData={setReturnDate} />
      </div>)}

      <div className={`form-group submit-group ${multiCity? "grid-break" : ""}`}>
         <div className="submit-group_inputs">

      <CustomSelectionInput width="100%" 
         title="Select nationality" data={dataNationality} 
         setData={setNationality} selectionData={sampleCityList}/>
      <CustomPeopleSelection width="100%" data={dataPeople} 
         setData={setPeople}/>
         </div>
      <button className={`internation-form_submit ${multiCity? "row-span-2" : ""}`}>
         SEARCH FLIGHT&nbsp;&nbsp;&gt;
         </button>
      </div>
   </form>;
};

export default InternationalForm;
