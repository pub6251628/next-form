import * as React from "react";
import {useState} from "react"
type CustomDateSelectionProps = {
  title:string, data:any, setData:any, width?: string
}
const CustomDateSelection = ({title, data, setData, }:CustomDateSelectionProps) => {
   const [isFocus, setIsFocus] = useState(false);
   const focusHandler = () => {
      setIsFocus((pre) => !pre);
   }
   const handleValueChange = (e: React.ChangeEvent<HTMLInputElement>) => {
         setData(e.target.value);
   }
  return (
    <div className='selection-input'>
      <label
        className={`selection-input_label ${data === "" && !isFocus? 'selection-input_label_show' : ''}`}
      >
        {title}
      </label>
      <div className={`selection-input_input ${isFocus? "selection-input_focus" : ""}`}>
         <input className='selection-input_date w-full' type='date'
            placeholder={data ? '' : title}
            name={title} onFocus={focusHandler} onBlur={focusHandler} 
            onChange={handleValueChange}
            value={data}/>
      </div>
    </div>
  )
}

export default CustomDateSelection