import * as React from "react";
import {useState} from "react"
import {DownOutlined, UpOutlined} from "@ant-design/icons"

type CustomSelectionInputProps = {
   selectionData:Array<String>, title:string, data:any, setData:any, width:string, className?:string
}

const CustomSelectionInput = ({selectionData, title, data, setData, width, className}:CustomSelectionInputProps) => {
   const [isFocus, setIsFocus] = useState(false);
   const focusHandler = () => {
      setIsFocus((pre) => !pre);
   }
   const handleValueChange = (e:React.MouseEvent<HTMLDivElement>) => {
      setData(e.currentTarget.innerText)
      setIsFocus(false)
   }
  return (
    <div className={`selection-input ${className}`}>
      <div className={`selection-input_input ${isFocus? "selection-input_focus" : ""}`} 
         onBlurCapture={focusHandler}>
         <input style={{width: width}} placeholder={title} 
         name={title} onFocus={focusHandler} value={data}
         spellCheck="false" />
         <div className='selection-input_icon' style={{color: "#ccc", marginRight: "5px" , 
            display:""}}>
            {isFocus? <UpOutlined /> : <DownOutlined />}
         </div>
      </div>
      <div className={`selection-input_selection ${isFocus? "show-selection" : 
         "hide-selection"}`}>
         {selectionData.map((i)=> 
         <div className='selection_option' onClick={handleValueChange}>{i}</div> )}
      </div>
    </div>
  )
}

export default CustomSelectionInput