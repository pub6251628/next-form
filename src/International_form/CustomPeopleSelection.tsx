import * as React from "react";
import {useState} from "react"
import {DownOutlined, UpOutlined, PlusOutlined, MinusOutlined} from "@ant-design/icons"
type data = {
   adult: number
   child:number,
}
type CustomPeopleSelectionProps = {
   data:data, 
   setData:any, width:string, className?:string
}
const CustomPeopleSelection = ({data, setData, width, className}:CustomPeopleSelectionProps) => {
   const [isFocus, setIsFocus] = useState(false);
   const [isMouseIn, setIsMouseIn] = useState(false);
   const focusHandler = () => {
      setIsFocus((pre) => !pre);
   }
   
   const addAdult = () => {
      setData((pre:data) => ({...pre, adult: pre.adult + 1}));
   }
   const addClild = () => {
      setData((pre:data) => ({...pre, child: pre.child + 1}));
   }
   const removeAdult = () => {
      setData((pre:data) => pre.adult > 1? ({...pre, adult: pre.adult - 1}) : pre)
   }
   const removeChild = () => {
      setData((pre:data) => pre.child > 0? ({...pre, child: pre.child - 1}) : pre)
   }

   const handleMouseEvent = () => {
      setIsMouseIn((pre) => !pre)
   } 
  return (
    <div className={`selection-input ${className}`}>
      <div className={`selection-input_input ${isFocus? "selection-input_focus" : ""}`} 
         onBlurCapture={focusHandler}>
         <input style={{width: width}} onFocus={focusHandler} 
         value={`${data.adult} Adult ${data.child > 0 ? "& "+data.child+" child" : ""}`}
         spellCheck="false" />
         <div className='selection-input_icon' style={{color: "#ccc", marginRight: "5px" ,
             display:""}}>
            {isFocus? <UpOutlined /> : <DownOutlined />}
         </div>
      </div>
      <div 
      className={`selection-input_selection people-selection_body ${isFocus || isMouseIn? 
         "show-selection" : "hide-selection"}`}
      onMouseEnter={handleMouseEvent} onMouseLeave={handleMouseEvent}>
         <div className='people-selection_column'>

            <div className='people-selection_item space-between'>
               <div>
                  <p className='people-selection-title'>Adult</p>
                  <p className='people-selection-sub'>12 years and above</p>
               </div>
               <div className='flex flex-row people-selection_control'>
                  <div className='people-selection_remove' onClick={removeAdult}>
                     <MinusOutlined />
                  </div>
                  <div className='people-selection_text'>{data.adult}</div>
                  <div className='people-selection_add' onClick={addAdult}> 
                     <PlusOutlined />
                  </div>
               </div>
            </div>

            <div className='people-selection_item space-between'>
               <div>
                  <p className='people-selection-title'>Child</p>
                  <p className='people-selection-sub'>Between 2 and 12 years</p>
               </div>
               <div className='flex flex-row people-selection_control'>
                  <div className='people-selection_remove' onClick={removeChild}>
                     <MinusOutlined />
                  </div>
                  <div className='people-selection_text'>{data.child}</div>
                  <div className='people-selection_add' onClick={addClild}> 
                     <PlusOutlined />
                  </div>
               </div>
            </div>

         </div>
      </div>
    </div>
  )
}

export default CustomPeopleSelection