import * as React from "react";
import { CloseOutlined, PlusOutlined } from "@ant-design/icons";
import CustomDateSelection from './CustomDateSelection';
import CustomSelectionInput from './CustomSelectionInput';

type MulticityDataRow = {from:string, to:string, date: string};
type MultiCityProps = {
  data: Array<MulticityDataRow>,
  setData: Function
}
const MultiCity = ({ data, setData }:MultiCityProps) => {
  const sampleCityList = [
    "New York","London","Paris","Tokyo","Sydney","Dubai","Rome","Berlin","Moscow","Toronto",
  ];
  return (
    <div className='flex flex-col gap-1'>
      {data.map((i, index) => (
        <div className='form-group multicity-formgroup' key={index}>
          <CustomSelectionInput width="100%" title="From" data={i.from}
            selectionData={sampleCityList}
            setData={(value:string) =>
              setData((prevData: Array<MulticityDataRow>) => {
                const newData = [...prevData];
                newData[index].from = value;
                return newData;
              })
            }
          />
          <CustomSelectionInput width="100%" title="To" data={i.to}
            selectionData={sampleCityList}
            setData={(value:string) =>
              setData((prevData:  Array<MulticityDataRow>) => {
                const newData = [...prevData];
                newData[index].to = value;
                return newData;
              })
            }
          />
          <CustomDateSelection width="100%" title="Date" data={i.date}
            setData={(value:string) =>
              setData((prevData: Array<MulticityDataRow>) => {
                const newData = [...prevData];
                newData[index].date = value;
                return newData;
              })
            }
          />
          <div
            className='remove-column'
            onClick={() => setData((prevData:  Array<MulticityDataRow>) => 
              prevData.filter((_, idx) => idx !== index))}
          >
            <CloseOutlined />
          </div>
        </div>
      ))}
      <div className='add-column' onClick={() => 
        setData((prevData:  Array<MulticityDataRow>) => [...prevData, { from: '', to: '', date: '' }])}>
        <span><PlusOutlined /></span> Add another flight
      </div>
    </div>
  );
};

export default MultiCity;
