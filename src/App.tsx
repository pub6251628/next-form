import * as React from "react";
import {useState} from "react"
import "./App.css";
import {GlobalOutlined, HomeOutlined } from "@ant-design/icons"
import InternationalForm from "./International_form/InternationalForm";
function App() {
  const [activeTab, setActiveTab] = useState("Interantional");

  const tabClicked = (e : React.MouseEvent<HTMLDivElement>) => {
    // set active tab
    setActiveTab(e.currentTarget.innerText);
  };

  // border bottm sytle based on current tab
  const domesticBorderBottom = {
    borderBottom:
      activeTab === "Domestic" ? "2px solid blue" : "2px solid rgba(0,0,0,0)",
  };
  const internationalBorderBottom = {
    borderBottom:
      activeTab === "Interantional" ? "2px solid blue": "2px solid rgba(0,0,0,0)",
  };
  return (
    <div className="app">
      <div className="main_card">
        <div className="main_card_title">Where to?</div>
        <div className="main_card_body">
          {/* Card head */}
          <div className="card_head">
            <div style={domesticBorderBottom} 
              className={`card_head_item ${activeTab === "Domestic"? "acitve-tab":""}`} 
              onClick={tabClicked}>
            <span className="tab-heading_icon"><HomeOutlined/></span>Domestic
            </div>
            <div style={internationalBorderBottom} 
              className={`card_head_item ${activeTab === "Interantional"? "acitve-tab":""}`} 
              onClick={tabClicked}>
              <span className="tab-heading_icon"><GlobalOutlined/></span>Interantional
            </div>
				<hr className="card_head_devider" />
          </div>
          {/* Card body */}
          <div className="card_body">
				{/* Load form based on the tab */}
            {activeTab === "Interantional" ? 
					(<InternationalForm />) : 
					("Domestic Form")
				}
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
